import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.ColorChooserUI;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Stern1 extends JFrame {

	private JPanel contentPane;
	public JTextField txtHierBitteText;
	private JLabel lblNewLabel = new JLabel("New label");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Stern1 frame = new Stern1();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Stern1() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 420, 632);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnRot = new JButton("Rot");
		btnRot.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
					buttonRotB_clicked();
			}
		});
		btnRot.setBounds(0, 124, 124, 37);
		contentPane.add(btnRot);
		
		JButton btnGrn = new JButton("Gr\u00FCn");
		btnGrn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
					buttonGreenB_clicked();
			}
		});
		btnGrn.setBounds(134, 124, 124, 37);
		contentPane.add(btnGrn);
		
		JButton btnBlau = new JButton("Blau");
		btnBlau.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonBlauB_clicked();
			}
		});
		btnBlau.setBounds(270, 124, 124, 37);
		contentPane.add(btnBlau);
		
		JButton btnGelb = new JButton("Gelb");
		btnGelb.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGreenB_clicked();
			}
		});
		btnGelb.setBounds(0, 172, 124, 37);
		contentPane.add(btnGelb);
		
		JButton btnStandardfarbe = new JButton("Standardfarbe");
		btnStandardfarbe.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonStandard_clicked();
			}
		});
		btnStandardfarbe.setBounds(134, 172, 124, 37);
		contentPane.add(btnStandardfarbe);
		
		JButton btnFarbeWhlen = new JButton("Farbe W\u00E4hlen");
		btnFarbeWhlen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonWahl_clicked();
			}
		});
		btnFarbeWhlen.setBounds(270, 172, 124, 37);
		contentPane.add(btnFarbeWhlen);
		
		JLabel lblAufgabe = new JLabel("Aufgabe 1: Hintergrundfarbe \u00E4ndern");
		lblAufgabe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabe.setBounds(0, 102, 222, 21);
		contentPane.add(lblAufgabe);
		
		JLabel lblAufgabeText = new JLabel("Aufgabe 2: Text formatieren ");
		lblAufgabeText.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabeText.setBounds(0, 220, 222, 21);
		contentPane.add(lblAufgabeText);
		
		JButton btnArial = new JButton("Arial");
		btnArial.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonArial_clicked();
			}
		});
		btnArial.setBounds(0, 252, 124, 21);
		contentPane.add(btnArial);
		
		JButton ComicSans = new JButton("Comic Sans MS");
		ComicSans.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonComicSans_clicked();
			}
		});
		ComicSans.setBounds(134, 252, 124, 21);
		contentPane.add(ComicSans);
		
		JButton courierNew = new JButton("Courier New");
		courierNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				buttonCourierNew_clicked();
			}
		});
		courierNew.setBounds(270, 252, 124, 21);
		contentPane.add(courierNew);
		
		txtHierBitteText = new JTextField();
		txtHierBitteText.setText("Hier bitte Text eingeben");
		txtHierBitteText.setBounds(0, 284, 394, 20);
		contentPane.add(txtHierBitteText);
		txtHierBitteText.setColumns(10);
		
		JButton btnInsLabelSchreiben = new JButton("Ins Label schreiben");
		btnInsLabelSchreiben.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonInsLabelSchreiben_clicked();
			}
		});
		btnInsLabelSchreiben.setBounds(0, 305, 196, 23);
		contentPane.add(btnInsLabelSchreiben);
		
		JButton btnTextImLabel = new JButton("Text im Label l\u00F6schen");
		btnTextImLabel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonloschen_clicked();
			}
		});
		btnTextImLabel.setBounds(198, 305, 196, 23);
		contentPane.add(btnTextImLabel);
		
		JLabel lblAufgabeSchriftfarbe = new JLabel("Aufgabe 3: Schriftfarbe \u00E4ndern");
		lblAufgabeSchriftfarbe.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabeSchriftfarbe.setBounds(0, 330, 222, 21);
		contentPane.add(lblAufgabeSchriftfarbe);
		
		JButton btnRot_1 = new JButton("Rot");
		btnRot_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonRot_clicked();
			}
		});
		btnRot_1.setBounds(0, 362, 124, 37);
		contentPane.add(btnRot_1);
		
		JButton btnBlau_1 = new JButton("Blau");
		btnBlau_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonBlau_clicked();
			}
		});
		btnBlau_1.setBounds(134, 362, 124, 37);
		contentPane.add(btnBlau_1);
		
		JButton btnSchwarz = new JButton("Schwarz");
		btnSchwarz.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonSchwarz_clicked();
			}
		});
		btnSchwarz.setBounds(270, 362, 124, 37);
		contentPane.add(btnSchwarz);
		
		JLabel lblAufgabeSchriftgre = new JLabel("Aufgabe 4: Schriftgr\u00F6\u00DFe \u00E4ndern");
		lblAufgabeSchriftgre.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabeSchriftgre.setBounds(0, 410, 222, 21);
		contentPane.add(lblAufgabeSchriftgre);
		
		JButton schriftGröser = new JButton("+");
		schriftGröser.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonGroser_clicked();
			}
		});
		schriftGröser.setBounds(0, 438, 196, 23);
		contentPane.add(schriftGröser);
		
		JButton schriftKleiner = new JButton("-");
		schriftKleiner.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				buttonkleiner_clicked();
			}
		});
		schriftKleiner.setBounds(198, 438, 196, 23);
		contentPane.add(schriftKleiner);
		
		JLabel lblAufgabeTextausrichtung = new JLabel("Aufgabe 5: Textausrichtung");
		lblAufgabeTextausrichtung.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabeTextausrichtung.setBounds(0, 462, 222, 21);
		contentPane.add(lblAufgabeTextausrichtung);
		
		JButton btnLinksbndig = new JButton("linksb\u00FCndig");
		btnLinksbndig.setBounds(0, 485, 124, 21);
		contentPane.add(btnLinksbndig);
		
		JButton btnZentriert = new JButton("zentriert");
		btnZentriert.setBounds(134, 484, 124, 21);
		contentPane.add(btnZentriert);
		
		JButton btnRechtsbndig = new JButton("rechtsb\u00FCndig");
		btnRechtsbndig.setBounds(270, 484, 124, 21);
		contentPane.add(btnRechtsbndig);
		
		JLabel lblAufgabeProgramm = new JLabel("Aufgabe 6: Programm beenden");
		lblAufgabeProgramm.setFont(new Font("Tahoma", Font.PLAIN, 13));
		lblAufgabeProgramm.setBounds(0, 513, 222, 21);
		contentPane.add(lblAufgabeProgramm);
		
		JButton btnExit = new JButton("EXIT");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(ABORT);
			}
		});
		btnExit.setBounds(0, 534, 394, 59);
		contentPane.add(btnExit);
		
		
		lblNewLabel.setBounds(10, 40, 384, 37);
		contentPane.add(lblNewLabel);
		
		
	}
	public void buttonkleiner_clicked() {
		int grose = lblNewLabel.getFont().getSize();
		lblNewLabel.setFont(new Font(lblNewLabel.getFont().getFontName(), Font.PLAIN, grose -1));
		
	}
	public void buttonGroser_clicked() {
		int grose = lblNewLabel.getFont().getSize();
		lblNewLabel.setFont(new Font(lblNewLabel.getFont().getFontName(), Font.PLAIN, grose +1));
		
	}

	public void buttonBlau_clicked() {
		this.lblNewLabel.setForeground(Color.BLUE);
		
	}	
	public void buttonSchwarz_clicked() {
		this.lblNewLabel.setForeground(Color.BLACK);
		
	}
	public void buttonRot_clicked() {
		this.lblNewLabel.setForeground(Color.RED);
		
	}

	public void buttonloschen_clicked() {
		lblNewLabel.setText("");
		
	}

	public void buttonInsLabelSchreiben_clicked() {
		lblNewLabel.setText(txtHierBitteText.getText());
		
	}

	public void buttonCourierNew_clicked() {
		lblNewLabel.setFont(new Font("Courier New", Font.PLAIN, 12));
		
	}

	public void buttonComicSans_clicked() {
		lblNewLabel.setFont(new Font("Comic Sans MS", Font.PLAIN, 12));
		
	}

	public void buttonArial_clicked() {
		lblNewLabel.setFont(new Font("Arial", Font.PLAIN, 12));
		
	}

	public void buttonBlauB_clicked() {
		this.contentPane.setBackground(Color.blue);
		
	}

	public void buttonGreenB_clicked() {
		this.contentPane.setBackground(Color.GREEN);
		
	}

	public void buttonRotB_clicked() {
		this.contentPane.setBackground(Color.red);
		
	}
	public void buttonGelbB_clicked() {
		this.contentPane.setBackground(Color.yellow);
		
	}	
	public void buttonStandard_clicked() {
		this.contentPane.setBackground(Color.white);
		
	}	
	public void buttonWahl_clicked() {
		JColorChooser cc = new JColorChooser(contentPane.getBackground()); 
		Color fw = cc.showDialog(null, "Farbe Wählen", null);
		contentPane.setBackground(fw);
	}
}
